########################Import############################
import numpy as np
import cv2
import utlis
import common
import os
import tensorflow.compat.v1 as tf

########################Labels (ne pas toucher)############################
labels={
 1: "person",
 2: "bicycle",
 3: "car",
 4: "motorcycle",
 5: "airplane",
 6: "bus",
 7: "train",
 8: "truck",
 9: "boat",
 10: "traffic light",
 11: "fire hydrant",
 13: "stop sign",
 14: "parking meter",
 15: "bench",
 16: "bird",
 17: "cat",
 18: "dog",
 19: "horse",
 20: "sheep",
 21: "cow",
 22: "elephant",
 23: "bear",
 24: "zebra",
 25: "giraffe",
 27: "backpack",
 28: "umbrella",
 31: "handbag",
 32: "tie",
 33: "suitcase",
 34: "frisbee",
 35: "skis",
 36: "snowboard",
 37: "sports ball",
 38: "kite",
 39: "baseball bat",
 40: "baseball glove",
 41: "skateboard",
 42: "surfboard",
 43: "tennis racket",
 44: "bottle",
 46: "wine glass",
 47: "cup",
 48: "fork",
 49: "knife",
 50: "spoon",
 51: "bowl",
 52: "banana",
 53: "apple",
 54: "sandwich",
 55: "orange",
 56: "broccoli",
 57: "carrot",
 58: "hot dog",
 59: "pizza",
 60: "donut",
 61: "cake",
 62: "chair",
 63: "couch",
 64: "potted plant",
 65: "bed",
 67: "dining table",
 70: "toilet",
 72: "tv",
 73: "laptop",
 74: "mouse",
 75: "remote",
 76: "keyboard",
 77: "cell phone",
 78: "microwave",
 79: "oven",
 80: "toaster",
 81: "sink",
 82: "refrigerator",
 84: "book",
 85: "clock",
 86: "vase",
 87: "scissors",
 88: "teddy bear",
 89: "hair drier",
 90: "toothbrush"
}
########################Option Modifiable############################

cameraFeed= False #Utiliser la camera : True or False
videoPath = 'project_video.mp4' #Fichier ou se trouve la video test
cameraNo= 0 #Numero du port de la webcam : 0:WebCam Internen, 1 : WebCam Externe
frameWidth= 640 #Width de la fenetre
frameHeight = 480 #Height de la fenetre
MODEL_NAME='ssd_mobilenet_v2_coco_2018_03_29' #Model apprentissage pour model
PATH_TO_FROZEN_GRAPH=MODEL_NAME+'/frozen_inference_graph.pb' #model frozen Base: MODEL_NAME+'/frozen_inference_graph.pb'
color_infos=(255, 255, 0)

if cameraFeed:intialTracbarVals = [24,55,12,100] #  #wT,hT,wB,hB
else:intialTracbarVals = [42,63,14,87]   #wT,hT,wB,hB

########################Mise en marche de la webcam############################

if cameraFeed:#si tu veux utiliser la webcam
    cap = cv2.VideoCapture(cameraNo)
    cap.set(3, frameWidth)
    cap.set(4, frameHeight)
else:#si tu veux utiliser la video
    cap = cv2.VideoCapture(videoPath)

#set les variables
count=0
noOfArrayValues =10
global arrayCurve, arrayCounter
arrayCounter=0
arrayCurve = np.zeros([noOfArrayValues])
myVals=[]
utlis.initializeTrackbars(intialTracbarVals)

detection_graph=tf.Graph()
with detection_graph.as_default():
  od_graph_def=tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
    serialized_graph=fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

#demarrage de la boucle
with detection_graph.as_default():
    with tf.Session() as sess:
        ops=tf.get_default_graph().get_operations()
        all_tensor_names={output.name for op in ops for output in op.outputs}
        tensor_dict={}
        for key in [
            'num_detections', 'detection_boxes', 'detection_scores',
            'detection_classes', 'detection_masks']:
            tensor_name=key+':0'
            if tensor_name in all_tensor_names:
                tensor_dict[key]=tf.get_default_graph().get_tensor_by_name(tensor_name)
        if 'detection_masks' in tensor_dict:
            quit("Masque non géré")
        image_tensor=tf.get_default_graph().get_tensor_by_name('image_tensor:0')
        while True:
            success, img = cap.read() #lecture de la video pour la transformer en image
            #############################Detection Voiture##########################################
            tickmark=cv2.getTickCount()
            output_dict=sess.run(tensor_dict, feed_dict={image_tensor: np.expand_dims(img, 0)})
            nbr_object=int(output_dict['num_detections'])
            classes=output_dict['detection_classes'][0].astype(np.uint8)
            boxes=output_dict['detection_boxes'][0]
            scores=output_dict['detection_scores'][0]
            for objet in range(nbr_object):
                ymin, xmin, ymax, xmax=boxes[objet]
                if scores[objet]>0.30:
                    height, width=img.shape[:2]
                    xmin=int(xmin*width)
                    xmax=int(xmax*width)
                    ymin=int(ymin*height)
                    ymax=int(ymax*height)
                    cv2.rectangle(img, (xmin, ymin), (xmax, ymax), color_infos, 1)
                    txt="{:s}:{:3.0%}".format(labels[classes[objet]], scores[objet])
                    cv2.putText(img, txt, (xmin, ymin-5), cv2.FONT_HERSHEY_PLAIN, 1, color_infos, 2)
            fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
            cv2.putText(img, "FPS: {:05.2f}".format(fps), (10, 20), cv2.FONT_HERSHEY_PLAIN, 1, color_infos, 2)

            #############################Detection Route##########################################
            if cameraFeed== False:img = cv2.resize(img, (frameWidth, frameHeight), None) #si la video n est pas a la taille demander force la resize
            #pour avoir les image sur une fenetre
            imgWarpPoints = img.copy()
            imgFinal = img.copy()
            imgCanny = img.copy()
            #############################Creer la fenetre avec toute les image de debug##########################################
            imgUndis = utlis.undistort(img)
            imgThres,imgCanny,imgColor = utlis.thresholding(imgUndis)
            src = utlis.valTrackbars()
            imgWarp = utlis.perspective_warp(imgThres, dst_size=(frameWidth, frameHeight), src=src)
            imgWarpPoints = utlis.drawPoints(imgWarpPoints, src)
            imgSliding, curves, lanes, ploty = utlis.sliding_window(imgWarp, draw_windows=True)

            #############################Recherche des ligne blanche##########################################
            try:
                #zoomer sur le rectangle selectionner
                curverad =utlis.get_curve(imgFinal, curves[0], curves[1])
                lane_curve = np.mean([curverad[0], curverad[1]])
                imgFinal = utlis.draw_lanes(img, curves[0], curves[1],frameWidth,frameHeight,src=src)

                # ## Average
                currentCurve = lane_curve // 50
                if  int(np.sum(arrayCurve)) == 0:averageCurve = currentCurve
                else:
                    averageCurve = np.sum(arrayCurve) // arrayCurve.shape[0]
                if abs(averageCurve-currentCurve) >200: arrayCurve[arrayCounter] = averageCurve
                else :arrayCurve[arrayCounter] = currentCurve
                arrayCounter +=1

                #calculer ou doit t il tourner
                if arrayCounter >=noOfArrayValues : arrayCounter=0
                turn = "^"
                if  int(averageCurve) > 10:
                    turn = "->"
                elif  int(averageCurve) < -9:
                    turn = "<-"
                else:
                    turn = " ^"
                
                #Afficher sur l'ecran
                cv2.putText(imgFinal, str(int(averageCurve)), (frameWidth//2-70, 70), cv2.FONT_HERSHEY_DUPLEX, 1.75, (0, 0, 255), 2, cv2.LINE_AA)
                cv2.putText(imgFinal, turn, (frameWidth//2-60, 110), cv2.FONT_HERSHEY_DUPLEX, 1.75, (0, 0, 255), 2, cv2.LINE_AA)
            except:
                lane_curve=00
                pass

            #Ouvrir les fenetres
            imgThres = cv2.cvtColor(imgThres,cv2.COLOR_GRAY2BGR)
            imgBlank = np.zeros_like(img)
            imgStacked = utlis.stackImages(0.7, ([img,imgUndis,imgWarpPoints],
                                                [imgColor, imgCanny, imgThres],
                                                [imgWarp,imgSliding,imgFinal]
                                                ))

            cv2.imshow("PipeLine",imgStacked)
            cv2.imshow("Result", imgFinal)
            #Arreter la boucle si q preser
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
#enlever tout ce qui est enregistrer dans la ram et fermer les fenetres
cap.release()
cv2.destroyAllWindows()
